# Kong Tools

A simple but extensible interface layer for adding custom elements to kongregate.com's web page/interface. 

In it's current form it might be more interesting for JavaScript/Tampermonkey developers.

**Talk to lee8oi if you have any questions.**

## Features

- Pure JS - Works without extra dependancies.
- Simple extension constructor style for extending Kong with new features.
- Standard features implemented as 'extensions' for modularity and structural purposes.

## Current Extensions:

- **monitor**: Adds an observer system for monitoring chat events (such as template change).

- **output**: Adds a custom output element for printing messages to.

- **input**: Adds a custom text input element that interfaces with Kongregate.coms's chat controls and prints to **output**.

- **command**: Extends the **input** extension to use a simple IRC style command system.


## Creating Extensions:

An extension is an object constructor that is used to extend the kong core with new features.

Each extension should be contained in it's own object and use its own kong node for publicly accessible methods, properties, etc. This helps keep the extensions modular and the kong structure clean & manageable.

kong.load essentially does the following with each extension constructor:
- intialize core extension object
- create a new kong.node
- run object.setup on the new kong.node

The kong object itself is passed to the constructor during initialization to give extensions full access to kong's public methods, properties, etc. Useful for overriding/integrating with other extensions, etc.