// ==UserScript==
// @name         Kong Tools
// @license      MPL-2.0
// @namespace    https://gitlab.com/lee8oi/kongtools
// @supportURL   https://gitlab.com/lee8oi/kongtools/issues
// @updateURL    https://gitlab.com/lee8oi/kongtools/raw/master/kong_update.user.js
// @downloadURL  https://gitlab.com/lee8oi/kongtools/raw/master/kong_tools.user.js
// @version      0.2.5
// @description  Custom tools for Kongregate users.
// @author       lee8oi@protonmail.com
// @match        https://www.kongregate.com/*
// @run-at       document-end
// @grant        GM_registerMenuCommand
// @grant        GM_addStyle
// @grant        GM_info
// ==/UserScript==

(function() {
    'use strict';

    console.log("Kong Tools ", GM_info.script.version);

    var k = new Kong(); // initialize kong object

    // load extenstions
    k.load(monitor_extension);
    k.load(output_extension);
    k.load(input_extension);
    k.load(command_extension);
    k.load(command_library);

    // assemble main elements
    k.extensions.output.main.appendChild(k.extensions.input.text);
    k.target.appendChild(k.extensions.output.main);

    k.extensions.monitor.templateChange();

    // show we're loaded
    k.extensions.output.print("Kong Tools " + k.version);

})();

/*
Kong core constructor
*/
function Kong() {
    var kong = this;

    kong.version = GM_info.script.version;

    // extensions node
    kong.extensions = {};

    // where to inject the kong elements
    kong.target = getXpathElem('//*[@id="play"]');

    // add a new node to kong object
    kong.new = function (name) {
        if (name) { // check for name
            kong.extensions[name] = {}; // create new node
            return kong.extensions[name];
        }
        console.log("extension error: missing name ");
        return null;
    };

    // load an extension
    kong.load = function (cons) {
        if (cons) {
            let ex = new cons(kong);
            if (ex) { // object initialized
                if (ex.setup) { // setup method exists
                    let name = ex.name, node = null;
                    if (name) { // if ex.name exists create new node
                        node = kong.new(name);
                    }
                    ex.setup(node);
                    if (node) {
                        if (ex.desc) { // description exists
                            node.desc = ex.desc;
                        }
                        if (node.styles) {
                            GM_addStyle(node.styles);
                        }
                        if (node.run) { // run method exists
                            node.run();
                        }
                    }
                    if (ex.name) {
                        console.log("extension loaded: ", ex.name);
                    }
                    return;
                } else {
                    console.log("extension error: missing setup method for ", ex.name);
                }
            } else {
                console.log("extension error: failed to initialize constructor");
            }
        } else {
            console.log("extension error: bad or missing constructor argument");
        }
    };
}

/*
Monitor extension

Adds an observer setup for monitoring kongregate chat events.
*/
function monitor_extension(kong) {
    var ob = this;

    ob.name = "monitor";
    ob.desc = "Observer methods for monitoring kongregate chat events.";
    ob.setup = function (node) {
        /* TODO:
        write message observer
        write userlist observer
        */
        // current template
        node.template = null;

        // monitor for template changes
        node.templateChange = function () {
            observeNode(getXpathElem('//*[@id="chat_container"]'), function(mlist, observer) {
                for (var m of mlist) {
                    if (m.type === "attributes" && m.target.className === "chat_room_template") {
                        if (m.target.style.display != "none") {
                            console.log("current template: ", m.target.style.display, m);
                            if (node.template != m.target) {
                                node.template = m.target;
                            }
                        }
                    }
                }
            });
        };
    }
}

/*
Output extension

Adds a simple output box for printing messages in.
*/
function output_extension(kong) {
    var ob = this;
    ob.name = "output";
    ob.desc = "Output display extension.";
    ob.setup = function (node) {

        // setup main elements
        node.main = document.createElement("span");
        node.main.id = "kh_output";
        node.text = document.createElement("textarea");
        node.text.id = "kh_output_text";
        node.main.appendChild(node.text);

        node.styles = `
span#kh_output {
position: fixed;
display: inline-block;
background: #222;
border-radius: 15px;
border: 1px solid white;
padding: 10px;
bottom: 0px;
right: calc(50% - 155px);
z-index: 100;
}
textarea#kh_output_text {
display: block;
width: 310px;
height: 75px;
border-radius: 5px;
padding: 5px;
margin-bottom: 10px;
}`;
        node.text.setAttribute("readonly", "");

        // print to output.text
        node.print = function (text) {
            node.text.value = node.text.value + text + "\n";
            node.text.scrollTop = node.text.scrollHeight;
        };
    };
}

/*
Input extension

Adds a text/chat element to send text input to the currently
active kongregate chat.
*/
function input_extension(kong) {
    var ob = this;
    ob.name = "input";
    ob.desc = "Text input extension.";
    ob.setup = function (node) {

        // setup main elements
        node.main = document.createElement("span");
        node.main.id = "kh_input";
        node.text = document.createElement("textarea");
        node.text.id = "kh_input_text";
        node.main.appendChild(node.text);

        // add default styles
        node.styles = `
span#kh_input {
margin-left: auto;
margin-right: auto;
display: inline-block;
background: #222;
border-radius: 15px;
border: 1px solid white;
padding: 10px;
}
textarea#kh_input_text {
border-radius: 5px;
padding: 5px;
width: 310px;
height: 15px;
}`;
        // clear text box
        node.clear = function () {
            node.text.value = "";
        };

        // handler for enter keypress listener
        node.keyupHandler = function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                node.sendChat(node.text.value);
                node.clear();
            }
        };

        // keypress listener
        node.text.addEventListener("keyup", node.keyupHandler);

        // chat template array
        node.chatTemplates = [];

        // get the current chat controls from active chat template
        node.getControls = function() {
            var t = kong.extensions.monitor.template;
            for (var c of t.childNodes) {
                if (c.className == "chat_controls") {
                    return c;
                }
            }
        };

        // send text to kongregate chat_controls and dispatch
        node.sendChat = function (text) {
            var t = text.trim();
            if (t.length > 0) {
                var c = node.getControls();
                for (var e of c.children) {
                    if (e.nodeName === "TEXTAREA") {
                        e.value = t;
                        e.dispatchEvent(enterKeyPress);
                    }
                }
            }
        }
    };
}

/*
Command extension

Extends the input extension with a simple command system.
*/
function command_extension(kong) {
    var ob = this;
    ob.name = "command";
    ob.desc = "Command support for the input extension.";
    ob.setup = function(node) {

        // create command lib node
        node.lib = {};

        // get command text from input element
        node.getCommand = function (elem) {
            var text = elem.value.trim();
            if (text.startsWith("/")) {
                var cmd = {};
                var m = text.match(/\/(\w+)\s([\S\s]*)/); // capture first word then capture everything else
                if (m) { // command and args captured
                    cmd.command = m[1].toLowerCase();
                    cmd.args = m[2];
                } else { // no args, just capture command instead
                    cmd.command = text.match(/\/(\w+)/)[1];
                }
                return cmd;
            }
            return false;
        };

        // keypress handler
        node.keyupHandler = function (event) {
            event.preventDefault();
            if (event.keyCode === 13) { // 13 == enter
                var cmd = node.getCommand(kong.extensions.input.text);
                if (!cmd) {
                    // not a command, run regular input handler
                    kong.extensions.input.keyupHandler(event);
                } else {
                    // execute the command
                    node.exec(cmd);
                }
                // clear input box
                kong.extensions.input.clear();
            }
        };

        // execute command
        node.exec = function(cmd) {
            var c = cmd.command.toLowerCase();
            if (node.lib[c]) {
                var args = cmd.args;
                if (args) {
                    node.lib[c].exec(args.trim());
                } else {
                    node.lib[c].exec("");
                }
            }
        };

        // extension info command
        node.lib.info = {};
        node.lib.info.desc = "Show description of specified extension.";
        node.lib.info.exec = function (args) {
            if (kong.extensions[args] && kong.extensions[args].desc) {
                kong.extensions.output.print(kong.extensions[args].desc);
            } else {
                let list = [];
                for (var n in kong.extensions) {
                    list.push(n);
                }
                kong.extensions.output.print(list.join(" "));
            }
        };

        // help info command
        node.lib.help = {};
        node.lib.help.desc = "Show description of specified command.";
        node.lib.help.exec = function (args) {
            if (node.lib[args] && node.lib[args].desc) {
                kong.extensions.output.print(node.lib[args].desc);
            } else {
                let list = [];
                for (var c in node.lib) {
                    if (c === "desc") {continue} // replace with default keyword check?
                    list.push(c);
                }
                kong.extensions.output.print(list.join(" "));
            }
        };

        // extension run method
        node.run = function (args) {
            kong.extensions.input.text.removeEventListener("keyup", kong.extensions.input.keyupHandler);
            kong.extensions.input.text.addEventListener("keyup", node.keyupHandler);
        }
    };
}

/*
Command library

Extends command extension with a library of commands.
*/
function command_library(kong) {
    var ob = this;

    ob.setup = function (node) {

        let lib = kong.extensions.command.lib;

        lib.slap = {};
        lib.slap.desc = "Slap someone with a large smelly trout!";
        lib.slap.exec = function (args) {
            if (args) {
                kong.extensions.input.sendChat("[ slaps " + args + " with a large smelly trout ]");
            }
        };

        lib.poke = {};
        lib.poke.desc = "Poke someone with a stick.";
        lib.poke.exec = function (args) {
            if (args) {
                kong.extensions.input.sendChat("[ pokes " + args + " with a stick ]");
            }
        };

        lib.fart = {};
        lib.fart.desc = "Let out a long horrendous fart and wave it towards someone!";
        lib.fart.exec = function (args) {
            if (args) {
                kong.extensions.input.sendChat("[ lets out a long horrendous fart and waves it towards " + args + " ]");
            } else {
                kong.extensions.input.sendChat("[ lets out a long horrendous fart ]");
            }
        };

        lib.approve = {};
        lib.approve.desc = "Nod approvingly to someone.";
        lib.approve.exec = function (args) {
            if (args) {
                kong.extensions.input.sendChat("[ nods approvingly at " + args + " ]");
            } else {
                kong.extensions.input.sendChat("[ nods approvingly ]");
            }
        };

        lib.disapprove = {};
        lib.disapprove.desc = "Shake your head disapprovingly at someone.";
        lib.disapprove.exec = function (args) {
            if (args) {
                kong.extensions.input.sendchat("[ shakes head disapprovingly at " + args + " ]");
            } else {
                kong.extensions.input.sendchat("[ shakes head disapprovingly ]");
            }
        };

        lib.fistbump = {};
        lib.fistbump.desc = "Fistbump someone to show respect.";
        lib.fistbump.exec = function (args) {
            if (args) {
                kong.extensions.input.sendChat("[ fistbumps " + args + " ]");
            } else {
                kong.extensions.input.sendChat("[ reaches out with a fistbump ]");
            }
        };
    };
}

/*
helper functions
*/

// enter key keypress event
const enterKeyPress = new KeyboardEvent("keypress", {
    bubbles: true, cancelable: true, keyCode: 13
});

// return the first element located at Xpath
function getXpathElem(path) {
    return document.evaluate(path, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
}

// observe node and call observerFunc on every mutation
function observeNode(targetNode, observerFunc) {
    if (targetNode) {
        //var config = { attributes: true, childList: true, subtree: true };
        var observer = new MutationObserver(observerFunc);
        observer.observe(targetNode, { attributes: true, childList: true, subtree: true });
    }
}
